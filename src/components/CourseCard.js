import {Card, CardImg} from 'react-bootstrap'
import {Link} from 'react-router-dom'
// import PropTypes from 'prop-types'


export default function CourseCard(course){

  // Destructuring the props
  const {name, description, price, image, _id} = course.courseProp

  // Initialize a 'count' state with a value of zero (0)
  // const [count, setCount] = useState(0) 
  // const [slots, setSlots] = useState(15)
  // const [isOpen, setIsOpen] = useState(true)

  // console.log({name})
  // console.log({description})
  // console.log({_id})

  // function enroll(){
  //  if(slots > 0){
  //    setCount(count + 1)
  //    setSlots(slots - 1)

  //    return 
  //  }

  //  alert('Slots are full!')
  // }

  // Effects in React is just like side effects/effects in real life, where everytime something happens within the component, a function or condition runs. 
  // You may also listen or watch a specific state for changes instead of watching/listening to the whole component
  // useEffect(() => {
  //  if(slots === 0){
  //    setIsOpen(false)
  //  }
  // }, [slots])

  return(
    <>
    <h1>{name}</h1>
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PHP {price}</Card.Text>
        <Card.Subtitle>Image</Card.Subtitle>
        <CardImg top src={image}/>
        <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
      </Card.Body>
    </Card>
    </>
  )
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them.
// {CourseCard.propTypes = {
//   course: PropTypes.shape({
//     name: PropTypes.string.isRequired,
//     description: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired
//   })
// }}