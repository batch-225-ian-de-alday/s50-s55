import React from 'react';


// Create a Context object

// Context object is a different approach in passing information between components and allows easier access by avoiding props-drilling

// type of object that can be used to store information that can be shared to other components within the app
const UserContext = React.createContext() // Base or data storage

// The "Provider" component allows other components to consume/use the context object and supply the neccessary information need to the context object
export const UserProvider = UserContext.Provider; // delivers the data to UserContext

export default UserContext;