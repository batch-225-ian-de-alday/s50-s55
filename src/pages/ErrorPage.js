import { Row, Col} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom';


export default function ErrorBanner() {
    return (
        <Row>
            <Col>
                <h1>Page Not Found</h1>
                <p>Go back to the <Link to="/">homepage</Link>.</p>
                <img src="https://i.imgur.com/qIufhof.png" alt="404 error"/>
            </Col>
        </Row>
    )
}

// Sir Cj solution
// return (
//     <>
//       <h2></h2>
//       <h3>Page Not Found</h3>
//       <p>
//         Go back to the <Link to="/">homepage</Link>
//       </p>
//     </>
//   );
// }