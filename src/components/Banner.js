// import Button from 'react-bootstrap/Button'
// import Row from 'react-bootstrap/Row'
// import Col from 'react-bootstrap/Col'


// Destructure importation
import { Button, Row, Col} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom';


export default function Banner() {
    return (
        <Row>
            <Col>
                <h1>Zuitt Coding Bootcamp</h1>
                <p>Opportunities for everyone, everywhere.</p>
                <Button as={Link} to='/login' variant="primary">Enroll now!</Button>
            </Col>
        </Row>
    )
}