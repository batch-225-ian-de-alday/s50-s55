import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';


// Import the bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// ======================================
// sample on printing on page
// const user = {
//   firstName: 'Luffy',
//   lastName: 'Monkey'
// };

// function formatName(user) {
//   return user.firstName + ' ' + user.lastName;
// }

// const element = <h1>Hello, {formatName(user)}</h1>

// const name = "John Smith";
// const element = <h1>Hello, {name}</h1>


// root.render(element)
